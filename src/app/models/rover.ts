import { Camera } from './camera';

export class Rover {

    cameras: Camera[];

    id: number;

    landingDate: string;

    launchDate: string;

    maxDate: string;

    maxSol: number;

    name: string;

    status: string;

    totalPhotos: number;
}
