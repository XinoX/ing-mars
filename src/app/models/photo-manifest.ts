export class Manifest {

    photo_manifest: PhotoManifest;
}

export class PhotoManifest {

    total_photos: number;
    photos: Photo[];
}

export class Photo {
    sol: number;
    total_photos: number;
}
