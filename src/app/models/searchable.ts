export interface Searchable {
    sol: number;
    camera: string;
    page: number;
    earthDate: string;
    rover: string;
}
