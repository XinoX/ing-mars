import { Camera } from './camera';
import { Rover } from './rover';

export class Photo {

    camera: Camera;

    earth_date: string;

    id: number;

    img_src: string;

    rover: Rover;

    sol: number;
}
