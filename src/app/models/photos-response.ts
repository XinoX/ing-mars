import { Photo } from './photo';

export class PhotosResponse {

    photos: Photo[];
}
