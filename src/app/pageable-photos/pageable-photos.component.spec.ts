import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageablePhotosComponent } from './pageable-photos.component';

describe('PageablePhotosComponent', () => {
  let component: PageablePhotosComponent;
  let fixture: ComponentFixture<PageablePhotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageablePhotosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageablePhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
