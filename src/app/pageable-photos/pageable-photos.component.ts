import { Component, OnInit } from '@angular/core';
import { Searchable } from '../models/searchable';
import { APP_URL, GET_PHOTOS, GET_MANIFEST } from '../constants/constants';
import { HttpClient, HttpParams } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { PhotosResponse } from '../models/photos-response';
import { Photo } from '../models/photo';
import { PhotoManifest, Manifest } from '../models/photo-manifest';

@Component({
  selector: 'app-pageable-photos',
  templateUrl: './pageable-photos.component.html',
  styleUrls: ['./pageable-photos.component.scss']
})
export class PageablePhotosComponent implements OnInit {

  search: Searchable = { rover: 'Curiosity', sol: 1000, camera: '', earthDate: '', page: 1 };
  photos: Photo[];
  totalPhotos: number;

  constructor(private http: HttpClient, private translate: TranslateService) { }

  ngOnInit() {
    this.photos = [];
    this.doSearch();
  }

  doSearch(pageNumber?: any) {
    const param = new HttpParams()
      .set('rover', this.search.rover)
      .set('sol', String(this.search.sol))
      .set('earthDate', this.search.earthDate)
      .set('camera', this.search.camera)
      .set('page', String(pageNumber || this.search.page));
    this.http.get<PhotosResponse>(APP_URL.concat(GET_PHOTOS), { params: param }).subscribe((data) => {
      this.photos = data.photos;
    });
    this.http.get<Manifest>(APP_URL.concat(GET_MANIFEST).concat(this.search.rover)).subscribe((data) => {
      const filtered = data.photo_manifest.photos.filter(photo => photo.sol === this.search.sol);
      if (filtered && filtered.length) {
        this.totalPhotos = filtered[0].total_photos;
      } else {
        this.totalPhotos = 0;
      }
    });
  }

  changeLocale(locale: string) {

    this.translate.use(locale);
  }

}
