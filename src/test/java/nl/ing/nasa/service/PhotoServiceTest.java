package nl.ing.nasa.service;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import nl.ing.nasa.config.ApplicationProperties;
import nl.ing.nasa.enumeration.Rover;
import nl.ing.nasa.repository.RequestLogRepository;

@SpringBootTest
public class PhotoServiceTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(PhotoServiceTest.class);

	@Autowired
	private RequestLogRepository requestLogRepo;

	@Autowired
	private ApplicationProperties applicationProperties;

	@Test
	public void getPhotos() {

		PhotoService sut = new PhotoService(applicationProperties, requestLogRepo);

		long oldCount = requestLogRepo.count();
		LOGGER.debug("Number of requests before fetching photos: {}", oldCount);

		sut.getPhotos(Rover.Curiosity, 1500, EMPTY, EMPTY, 1);

		long newCount = requestLogRepo.count();
		LOGGER.debug("Number of requests after fetching photos: {}", newCount);

		Assertions.assertEquals(++oldCount, newCount);
	}

}
