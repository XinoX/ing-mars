package nl.ing.nasa.util;

import static java.util.Arrays.asList;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsConfiguration;

import nl.ing.nasa.config.ApplicationProperties;

/**
 * Properties are configured in the application.yml file.
 */
@Component
public class NasaProperties {

	private final ApplicationProperties applicationProperties;

	public NasaProperties(ApplicationProperties applicationProperties) {
		this.applicationProperties = applicationProperties;
	}

	public static class Http {

		public static class Cache {

			private int timeToLiveInDays = NasaDefaults.Http.Cache.timeToLiveInDays;

			public int getTimeToLiveInDays() {
				return timeToLiveInDays;
			}

			public void setTimeToLiveInDays(int timeToLiveInDays) {
				this.timeToLiveInDays = timeToLiveInDays;
			}
		}

		private final Cache cache = new Cache();

		public Cache getCache() {
			return cache;
		}
	}

	private final CorsConfiguration cors = new CorsConfiguration();

	private final Http http = new Http();

	/**
	 * <p>
	 * Getter for the field <code>cors</code>.
	 * </p>
	 *
	 * @return a {@link org.springframework.web.cors.CorsConfiguration} object.
	 */
	public CorsConfiguration getCors() {
		return cors;
	}

	/**
	 * <p>
	 * Getter for the field <code>http</code>.
	 * </p>
	 *
	 * @return a {@link nl.ing.nasa.util.NasaProperties.Http} object.
	 */
	public Http getHttp() {
		return http;
	}

	@PostConstruct
	public void init() {
		cors.setAllowedOrigins(asList(applicationProperties.getCorsUrls()));
	}
}
