package nl.ing.nasa.util;

public interface NasaDefaults {

	interface Http {

		interface Cache {

			int timeToLiveInDays = 1461; // 4 years (including leap day)
		}
	}
}
