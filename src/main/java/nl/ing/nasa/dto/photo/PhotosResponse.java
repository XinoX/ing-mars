package nl.ing.nasa.dto.photo;

import java.io.Serializable;
import java.util.List;

public class PhotosResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Photo> photos;

	public List<Photo> getPhotos() {
		return photos;
	}

	public void setPhotos(List<Photo> photos) {
		this.photos = photos;
	}

	@Override
	public String toString() {
		return "PhotosResponse [photos=" + photos + "]";
	}

}
