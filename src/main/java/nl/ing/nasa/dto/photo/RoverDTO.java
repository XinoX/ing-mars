package nl.ing.nasa.dto.photo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RoverDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Camera> cameras;

	private Long id;

	@JsonProperty("landing_date")
	private String landingDate;

	@JsonProperty("launch_date")
	private String launchDate;

	@JsonProperty("max_date")
	private String maxDate;

	@JsonProperty("max_sol")
	private Long maxSol;

	private String name;

	private String status;

	@JsonProperty("total_photos")
	private Long totalPhotos;

	public List<Camera> getCameras() {
		return cameras;
	}

	public Long getId() {
		return id;
	}

	public String getLandingDate() {
		return landingDate;
	}

	public String getLaunchDate() {
		return launchDate;
	}

	public String getMaxDate() {
		return maxDate;
	}

	public Long getMaxSol() {
		return maxSol;
	}

	public String getName() {
		return name;
	}

	public String getStatus() {
		return status;
	}

	public Long getTotalPhotos() {
		return totalPhotos;
	}

	public void setCameras(List<Camera> cameras) {
		this.cameras = cameras;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLandingDate(String landingDate) {
		this.landingDate = landingDate;
	}

	public void setLaunchDate(String launchDate) {
		this.launchDate = launchDate;
	}

	public void setMaxDate(String maxDate) {
		this.maxDate = maxDate;
	}

	public void setMaxSol(Long maxSol) {
		this.maxSol = maxSol;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTotalPhotos(Long totalPhotos) {
		this.totalPhotos = totalPhotos;
	}

	@Override
	public String toString() {
		return "Rover [id=" + id + ", name=" + name + ", landingDate=" + landingDate + ", launchDate=" + launchDate
				+ ", status=" + status + ", maxDate=" + maxDate + ", totalPhotos=" + totalPhotos + ", maxSol=" + maxSol
				+ ", cameras=" + cameras + "]";
	}

}
