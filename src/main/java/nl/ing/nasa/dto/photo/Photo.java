package nl.ing.nasa.dto.photo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Photo implements Serializable {

	private static final long serialVersionUID = 1L;

	private Camera camera;
	
	@JsonProperty("earth_date")
	private String earthDate;
	
	private Long id;

	@JsonProperty("img_src")
	private String imgSrc;
	
	private RoverDTO rover;
	
	private Long sol;

	public Camera getCamera() {
		return camera;
	}

	public String getEarthDate() {
		return earthDate;
	}

	public Long getId() {
		return id;
	}

	public String getImgSrc() {
		return imgSrc;
	}

	public RoverDTO getRover() {
		return rover;
	}

	public Long getSol() {
		return sol;
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	public void setEarthDate(String earthDate) {
		this.earthDate = earthDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setImgSrc(String imgSrc) {
		this.imgSrc = imgSrc;
	}

	public void setRover(RoverDTO rover) {
		this.rover = rover;
	}

	public void setSol(Long sol) {
		this.sol = sol;
	}

	@Override
	public String toString() {
		return "Photo [id=" + id + ", sol=" + sol + ", camera=" + camera + ", imgSrc=" + imgSrc + ", earthDate="
				+ earthDate + ", rover=" + rover + "]";
	}
	

}
