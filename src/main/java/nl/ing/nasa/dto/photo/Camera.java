package nl.ing.nasa.dto.photo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Camera implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("full_name")
	private String fullName;

	private Long id;

	private String name;

	@JsonProperty("rover_id")
	private Long roverId;

	public String getFullName() {
		return fullName;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Long getRoverId() {
		return roverId;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRoverId(Long roverId) {
		this.roverId = roverId;
	}

	@Override
	public String toString() {
		return "Camera [id=" + id + ", name=" + name + ", roverId=" + roverId + ", fullName=" + fullName + "]";
	}

}
