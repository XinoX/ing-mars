package nl.ing.nasa.dto.manifest;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ManifestResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("photo_manifest")
	private PhotoManifest photoManifest;

	public PhotoManifest getPhotoManifest() {
		return photoManifest;
	}

	public void setPhotoManifest(PhotoManifest photoManifest) {
		this.photoManifest = photoManifest;
	}

	@Override
	public String toString() {
		return "ManifestResponse [photoManifest=" + photoManifest + "]";
	}

}
