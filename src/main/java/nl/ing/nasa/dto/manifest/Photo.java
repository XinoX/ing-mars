package nl.ing.nasa.dto.manifest;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import nl.ing.nasa.enumeration.Camera;

public class Photo implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Camera> cameras;

	@JsonProperty("earth_date")
	private String earthDate;

	private Long sol;

	@JsonProperty("total_photos")
	private Long totalPhotos;

	public List<Camera> getCameras() {
		return cameras;
	}

	public String getEarthDate() {
		return earthDate;
	}

	public Long getSol() {
		return sol;
	}

	public Long getTotalPhotos() {
		return totalPhotos;
	}

	public void setCameras(List<Camera> cameras) {
		this.cameras = cameras;
	}

	public void setEarthDate(String earthDate) {
		this.earthDate = earthDate;
	}

	public void setSol(Long sol) {
		this.sol = sol;
	}

	public void setTotalPhotos(Long totalPhotos) {
		this.totalPhotos = totalPhotos;
	}

	@Override
	public String toString() {
		return "Photo [cameras=" + cameras + ", earthDate=" + earthDate + ", sol=" + sol + ", totalPhotos="
				+ totalPhotos + "]";
	}
}
