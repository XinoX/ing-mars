package nl.ing.nasa.dto.manifest;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PhotoManifest implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("landing_date")
	private String landingDate;

	@JsonProperty("launch_date")
	private String launchDate;

	@JsonProperty("max_date")
	private String maxDate;

	@JsonProperty("max_sol")
	private float maxSol;

	private String name;

	private List<Photo> photos;

	private String status;

	@JsonProperty("total_photos")
	private float totalPhotos;

	public String getLandingDate() {
		return landingDate;
	}

	public String getLaunchDate() {
		return launchDate;
	}

	public String getMaxDate() {
		return maxDate;
	}

	public float getMaxSol() {
		return maxSol;
	}

	public String getName() {
		return name;
	}

	public List<Photo> getPhotos() {
		return photos;
	}

	public String getStatus() {
		return status;
	}

	public float getTotalPhotos() {
		return totalPhotos;
	}

	public void setLandingDate(String landingDate) {
		this.landingDate = landingDate;
	}

	public void setLaunchDate(String launchDate) {
		this.launchDate = launchDate;
	}

	public void setMaxDate(String maxDate) {
		this.maxDate = maxDate;
	}

	public void setMaxSol(float maxSol) {
		this.maxSol = maxSol;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPhotos(List<Photo> photos) {
		this.photos = photos;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTotalPhotos(float totalPhotos) {
		this.totalPhotos = totalPhotos;
	}

	@Override
	public String toString() {
		return "PhotoManifest [name=" + name + ", landingDate=" + landingDate + ", launchDate=" + launchDate
				+ ", status=" + status + ", maxSol=" + maxSol + ", maxDate=" + maxDate + ", totalPhotos=" + totalPhotos
				+ ", photos=" + photos + "]";
	}

}
