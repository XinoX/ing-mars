package nl.ing.nasa.web.rest;

import static org.springframework.http.HttpStatus.OK;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import nl.ing.nasa.dto.photo.PhotosResponse;
import nl.ing.nasa.enumeration.Rover;
import nl.ing.nasa.service.PhotoService;

@RestController
@RequestMapping("/api")
public class PhotoController {

	private final Logger LOGGER = LoggerFactory.getLogger(PhotoController.class);
	private PhotoService photoService;

	public PhotoController(PhotoService photoService) {
		this.photoService = photoService;
	}

	@GetMapping("/photos")
	public ResponseEntity<PhotosResponse> getPhotos(Rover rover, Integer sol, String earthDate, String camera,
			int page) {
		LOGGER.debug(
				"REST request to retrieve photos by criterias: rover: {}, sol: {}, earth date: {}, camera: {}, page: {}",
				rover.name(), sol, earthDate, camera, page);

		PhotosResponse response = photoService.getPhotos(rover, sol, earthDate, camera, page);
		return new ResponseEntity<PhotosResponse>(response, OK);
	}
}
