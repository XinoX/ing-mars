package nl.ing.nasa.web.rest;

import static org.springframework.http.HttpStatus.OK;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import nl.ing.nasa.dto.manifest.ManifestResponse;
import nl.ing.nasa.enumeration.Rover;
import nl.ing.nasa.service.ManifestService;

@RestController
@RequestMapping("/api")
public class ManifestController {

	private final Logger LOGGER = LoggerFactory.getLogger(ManifestController.class);
	private ManifestService manifestSrv;

	public ManifestController(ManifestService photoService) {
		this.manifestSrv = photoService;
	}

	@GetMapping("/manifests/{rover}")
	public ResponseEntity<ManifestResponse> getPhotos(@PathVariable Rover rover) {
		LOGGER.debug("REST request to retrieve manifests for rover: {}", rover.name());

		ManifestResponse response = manifestSrv.getManifests(rover);
		return new ResponseEntity<ManifestResponse>(response, OK);
	}
}
