package nl.ing.nasa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.ing.nasa.domain.RequestLog;

@Repository
public interface RequestLogRepository extends JpaRepository<RequestLog, Long> {

}
