package nl.ing.nasa.enumeration;

public enum Rover {
	Curiosity, Opportunity, Spirit
}
