package nl.ing.nasa.enumeration;

public enum Camera {

	FHAZ, RHAZ, MAST, CHEMCAM, MAHLI, MARDI, NAVCAM, PANCAM, MINITES, ENTRY

}
