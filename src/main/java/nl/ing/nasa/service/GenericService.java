package nl.ing.nasa.service;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

public abstract class GenericService {

	protected static final HttpEntity<?> ENTITY;
	protected static final HttpHeaders HEADERS = new HttpHeaders();
	protected static final String API_KEY = "api_key";
	protected static final RestTemplate REST = new RestTemplate();

	static {
		HEADERS.set("Accept", APPLICATION_JSON_VALUE);
		ENTITY = new HttpEntity<>(HEADERS);
	}

}
