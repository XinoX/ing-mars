package nl.ing.nasa.service;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.springframework.http.HttpMethod.GET;

import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import org.springframework.web.util.UriComponentsBuilder;

import nl.ing.nasa.config.ApplicationProperties;
import nl.ing.nasa.domain.RequestLog;
import nl.ing.nasa.dto.photo.PhotosResponse;
import nl.ing.nasa.enumeration.Rover;
import nl.ing.nasa.repository.RequestLogRepository;

/** TODO: Use Logger **/
@Service
public class PhotoService extends GenericService {

	private static final String METHOD = "GET_PHOTOS";
	private static final String PHOTOS_URL = "photos";
	private static final String ROVERS_URL = "rovers";

	private ApplicationProperties props;
	private RequestLogRepository requestLogRepo;

	public PhotoService(ApplicationProperties applicationProperties, RequestLogRepository requestLogRepo) {
		this.props = applicationProperties;
		this.requestLogRepo = requestLogRepo;
	}

	/** TODO: Persisting in database could be done using AOP **/
	public PhotosResponse getPhotos(Rover rover, Integer sol, String earthDate, String camera, int page) {

		String url = props.getNasaUrl().concat(String.join("/", ROVERS_URL, rover.name().toLowerCase(), PHOTOS_URL));
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
		setQueryParams(sol, earthDate, camera, page, builder);

		StopWatch watch = new StopWatch();
		watch.start();
		PhotosResponse result = REST.exchange(builder.toUriString(), GET, ENTITY, PhotosResponse.class).getBody();
		watch.stop();
		requestLogRepo.save(new RequestLog(-1L, METHOD, watch.getTotalTimeMillis(), new Date()));

		return result;
	}

	private void setQueryParams(Integer sol, String earthDate, String camera, int page, UriComponentsBuilder builder) {
		builder.queryParam(API_KEY, props.getApiKey()).queryParam("sol", sol).queryParam("page", page);
		if (isNotBlank(earthDate)) {
			builder.queryParam("earth_date", earthDate);
		}
		if (isNotBlank(camera)) {
			builder.queryParam("camera", camera);
		}
	}

}
