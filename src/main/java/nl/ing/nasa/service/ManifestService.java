package nl.ing.nasa.service;

import static org.springframework.http.HttpMethod.GET;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import org.springframework.web.util.UriComponentsBuilder;

import nl.ing.nasa.config.ApplicationProperties;
import nl.ing.nasa.domain.RequestLog;
import nl.ing.nasa.dto.manifest.ManifestResponse;
import nl.ing.nasa.enumeration.Rover;
import nl.ing.nasa.repository.RequestLogRepository;

@Service
@Transactional
public class ManifestService extends GenericService {

	private static final Map<Rover, UriComponentsBuilder> COMPONENT_BUILDER = new HashMap<Rover, UriComponentsBuilder>();
	private static final String MANIFEST_URL = "manifests/";
	private static final String METHOD = "GET_MANIFEST";

	private ApplicationProperties props;
	private RequestLogRepository requestLogRepo;

	public ManifestService(ApplicationProperties applicationProperties, RequestLogRepository requestLogRepo) {
		this.props = applicationProperties;
		this.requestLogRepo = requestLogRepo;
	}

	/** TODO: Persisting in database could be done using AOP **/
	public ManifestResponse getManifests(Rover rover) {

		UriComponentsBuilder builder = getUriComponentBuilder(rover);

		StopWatch watch = new StopWatch();
		watch.start();
		ManifestResponse result = REST.exchange(builder.toUriString(), GET, ENTITY, ManifestResponse.class).getBody();
		watch.stop();

		requestLogRepo.save(new RequestLog(-1L, METHOD, watch.getTotalTimeMillis(), new Date()));
		return result;
	}

	private UriComponentsBuilder getUriComponentBuilder(Rover rover) {
		if (COMPONENT_BUILDER.get(rover) != null) {
			return COMPONENT_BUILDER.get(rover);
		} else {
			UriComponentsBuilder builder = UriComponentsBuilder
					.fromHttpUrl(props.getNasaUrl().concat(MANIFEST_URL).concat(rover.name().toLowerCase()))
					.queryParam(API_KEY, props.getApiKey());
			return builder;
		}
	}

}
