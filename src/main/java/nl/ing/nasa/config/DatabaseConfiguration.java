package nl.ing.nasa.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("nl.ing.nasa.repository")
public class DatabaseConfiguration {

	private final Logger LOGGER = LoggerFactory.getLogger(DatabaseConfiguration.class);

	public DatabaseConfiguration(Environment env) {
		LOGGER.debug("Configuring JPA.");
	}

}
