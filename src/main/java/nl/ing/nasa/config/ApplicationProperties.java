package nl.ing.nasa.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to application.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
	
	private String apiKey;
	
	private String corsUrls;
	
	private String nasaUrl;

	public String getApiKey() {
		return apiKey;
	}

	public String getCorsUrls() {
		return corsUrls;
	}

	public String getNasaUrl() {
		return nasaUrl;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public void setCorsUrls(String corsUrls) {
		this.corsUrls = corsUrls;
	}

	public void setNasaUrl(String nasaUrl) {
		this.nasaUrl = nasaUrl;
	}
}
