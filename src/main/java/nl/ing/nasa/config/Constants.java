package nl.ing.nasa.config;

/**
 * Application constants.
 */
public final class Constants {

	/** Constant <code>SPRING_PROFILE_DEVELOPMENT="dev"</code> */
	public static final String SPRING_PROFILE_DEVELOPMENT = "dev";

	/** Constant <code>SPRING_PROFILE_TEST="test"</code> */
	public static final String SPRING_PROFILE_TEST = "test";

	/** Constant <code>SPRING_PROFILE_PRODUCTION="prod"</code> */
	public static final String SPRING_PROFILE_PRODUCTION = "prod";

	/**
	 * Spring profile used to disable running liquibase Constant
	 * <code>SPRING_PROFILE_NO_LIQUIBASE="no-liquibase"</code>
	 */
	public static final String SPRING_PROFILE_NO_LIQUIBASE = "no-liquibase";

	private Constants() {
		throw new RuntimeException();
	}
}
