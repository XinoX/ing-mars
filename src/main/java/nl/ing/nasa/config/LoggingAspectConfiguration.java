package nl.ing.nasa.config;

import static nl.ing.nasa.config.Constants.SPRING_PROFILE_DEVELOPMENT;

import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

import nl.ing.nasa.aop.logging.LoggingAspect;

@Configuration
@EnableAspectJAutoProxy
public class LoggingAspectConfiguration {

    @Bean
    @Profile(SPRING_PROFILE_DEVELOPMENT)
    public LoggingAspect loggingAspect(Environment env) {
        return new LoggingAspect(env);
    }
}
