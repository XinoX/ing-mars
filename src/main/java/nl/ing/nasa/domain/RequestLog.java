package nl.ing.nasa.domain;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Request Log.
 */
@Entity
@Table(name = "nasa_request_log")
public class RequestLog implements Serializable {

	public RequestLog() {
	}

	public RequestLog(Long id, String methodName, Long responseTime, Date createDate) {
		this.id = id;
		this.methodName = methodName;
		this.responseTime = responseTime;
		this.createDate = createDate;
	}

	private static final long serialVersionUID = 1L;

	@Column(name = "created_date")
	private Date createDate;

	@Id
	@SequenceGenerator(name = "sequenceGenerator")
	@GeneratedValue(strategy = SEQUENCE, generator = "sequenceGenerator")
	private Long id;

	@Column(name = "method_name", length = 50)
	private String methodName;

	@Column(name = "response_time")
	private Long responseTime;

	public Date getCreateDate() {
		return createDate;
	}

	public Long getId() {
		return id;
	}

	public String getMethodName() {
		return methodName;
	}

	public Long getResponseTime() {
		return responseTime;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public void setResponseTime(Long responseTime) {
		this.responseTime = responseTime;
	}

}
