package nl.ing.nasa;

import static nl.ing.nasa.config.Constants.SPRING_PROFILE_DEVELOPMENT;
import static nl.ing.nasa.config.Constants.SPRING_PROFILE_PRODUCTION;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;

import nl.ing.nasa.config.ApplicationProperties;
import nl.ing.nasa.config.DefaultProfileUtil;

@SpringBootApplication
@EnableConfigurationProperties({ LiquibaseProperties.class, ApplicationProperties.class })
public class NasaApplication implements InitializingBean {

	private static final Logger log = LoggerFactory.getLogger(NasaApplication.class);
	private static final String HTTP_PROTOCOL = "http";
	private static String HOST_ADDRESS = "localhost";

	private static void logApplicationStartup(Environment env) {

		String serverPort = env.getProperty("server.port");
		String contextPath = env.getProperty("server.servlet.context-path");
		if (StringUtils.isBlank(contextPath)) {
			contextPath = "/";
		}
		try {
			HOST_ADDRESS = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			log.warn("The host name could not be determined, using `localhost` as fallback");
		}
		log.info(
				"\n----------------------------------------------------------\n\t"
						+ "Application '{}' is running! Access URLs:\n\t" + "Local: \t\t{}://localhost:{}{}\n\t"
						+ "External: \t{}://{}:{}{}\n\t"
						+ "Profile(s): \t{}\n----------------------------------------------------------",
				env.getProperty("spring.application.name"), HTTP_PROTOCOL, serverPort, contextPath, HTTP_PROTOCOL,
				HOST_ADDRESS, serverPort, contextPath, env.getActiveProfiles());
	}

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(NasaApplication.class);
		DefaultProfileUtil.addDefaultProfile(app);
		Environment env = app.run(args).getEnvironment();
		logApplicationStartup(env);
	}

	private final Environment env;

	public NasaApplication(Environment env) {
		this.env = env;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
		if (activeProfiles.contains(SPRING_PROFILE_DEVELOPMENT) && activeProfiles.contains(SPRING_PROFILE_PRODUCTION)) {
			log.error("You have misconfigured your application! It should not run "
					+ "with both the 'dev' and 'prod' profiles at the same time.");
		}
	}

}
