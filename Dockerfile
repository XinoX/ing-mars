#
# Build stage
#
FROM maven:3.6.2-jdk-8-slim AS build
RUN useradd -d /home/newuser -m -s /bin/bash newuser
RUN mkdir -p /src
RUN chown newuser /src
USER newuser
COPY . /src
WORKDIR /src
RUN mvn clean package -DskipTests

#
# Package stage
#
FROM openjdk:8-jre-slim
COPY --from=build /src/target/nasa-0.0.1-SNAPSHOT.jar /usr/local/lib/app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/local/lib/app.jar"]